﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VTExperiment1
{
    /// <summary>
    /// Interaction logic for Condition.xaml
    /// </summary>
    public partial class Condition : Window
    {
        bool PFilled = false;
        bool BLFilled = false;
        bool OFilled = false;
        public Condition()
        {
            InitializeComponent();
            foreach (string s in Enum.GetNames(typeof(BodyLocations)))
                BodyLocationBox.Items.Add(s);
            foreach (string s in Enum.GetNames(typeof(Orientations)))
                OrientationBox.Items.Add(s);
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Logger.logger = new LogWriter(ParticipantTextField.Text, BodyLocationBox.SelectedValue.ToString() , OrientationBox.SelectedValue.ToString());
            Close();
        }

        private void ParticipantTextField_TextChanged(object sender, TextChangedEventArgs e)
        {
            PFilled = true;
            if (PFilled & BLFilled & OFilled)
                SaveButton.IsEnabled = true;
        }

        private void BodyLocationBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BLFilled = true;
            if (PFilled & BLFilled & OFilled)
                SaveButton.IsEnabled = true;
        }

        private void OrientationBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OFilled = true;
            if (PFilled & BLFilled & OFilled)
                SaveButton.IsEnabled = true;
        }
    }

    public static class Logger
    {
        public static LogWriter logger;
    }

    enum BodyLocations
    {
        Wrist,
        Forearm,
        UpperArm,
        Back,
        Stomach,
        Thigh,
        Leg
    }

    enum Orientations
    {
        Along,
        Round
    }
}
