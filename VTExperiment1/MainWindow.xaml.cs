﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Collections;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ActuBoardAPI;

namespace VTExperiment1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ActuBoardInterface _abi;
        double[] calibratedIntensities;
        int reversal_ctr = 0;
        int distance = 5;
        int reversal_flag = 0;
        int step = 1;
        Tuple<int, int>[] tuples =
            {   Tuple.Create(4, 5), // 1 unit distance
                Tuple.Create(3, 5), // 2 units
                Tuple.Create(3, 6),
                Tuple.Create(2, 6),
                Tuple.Create(2, 7),
                Tuple.Create(1, 7),
                Tuple.Create(1, 8),
                Tuple.Create(0, 8),
                Tuple.Create(0, 9)
        };
        Tuple<int, int>[] tuples6 =
            {
                Tuple.Create(2, 3), // 1 unit distance
                Tuple.Create(2, 4), // 2 units
                Tuple.Create(1, 4),
                Tuple.Create(1, 5),
                Tuple.Create(0, 5)
            };

    public MainWindow()
        {
            InitializeComponent();
            //Console.WriteLine(_abi.SetChannel(45, 125));
            Calibration calibration = new Calibration();
            calibration.ShowDialog();
            calibration.BringIntoView();
            calibratedIntensities = calibration.GetCalibrationData();
            foreach (double d in calibratedIntensities)
                Console.Write(" " + d);
            _abi = new ActuBoardInterface();
            _abi.SetComPort(6);
            _abi.Connect();
            _abi.EnableOutput(true);
        }

        private void DISTINCT_Click(object sender, RoutedEventArgs e)
        {
            Logger.logger.LogWrite("1 Point" + distance);
            if (reversal_flag == 1)
            {
                reversal_ctr += 1;
                reversal_flag = 0;
                Logger.logger.LogWrite("REVERSAL " + reversal_ctr);
            }
            if (distance - step > 0)
            {
                distance = distance - step;
            }
            //Vibrate();
        }

        private void NONDISTINCT_Click(object sender, RoutedEventArgs e)
        {
            Logger.logger.LogWrite("Several Points" + distance);
            if (reversal_flag == 0)
            {
                reversal_ctr += 1;
                reversal_flag = 1;
                Logger.logger.LogWrite("REVERSAL " + reversal_ctr);
            }
            if (distance + step < 6)
            {
                distance = distance + step;
            }
            //Vibrate();
        }

        private void VIBRATE_Click(object sender, RoutedEventArgs e)
        {
            Vibrate();
        }

        private void Vibrate()
        {   if(Logger.logger.BodyLocation == "Wrist")
            {
                int m1 = tuples6[distance].Item1;
                int m2 = tuples6[distance].Item2;
                _abi.SetChannel(m1 + 40, (int)((calibratedIntensities[m1] / 50) * 165));
                _abi.SetChannel(m2 + 40, (int)((calibratedIntensities[m2] / 50) * 165));
                System.Threading.Thread.Sleep(1000);
                _abi.SetChannel(m1 + 40, 0);
                _abi.SetChannel(m2 + 40, 0);
            }
            else
            {
                int m1 = tuples[distance].Item1;
                int m2 = tuples[distance].Item2;
                _abi.SetChannel(m1 + 40, (int)((calibratedIntensities[m1] / 50) * 165));
                _abi.SetChannel(m2 + 40, (int)((calibratedIntensities[m2] / 50) * 165));
                System.Threading.Thread.Sleep(1000);
                _abi.SetChannel(m1 + 40, 0);
                _abi.SetChannel(m2 + 40, 0);

            }
            if (reversal_ctr == 6)
            {
                Close();
            }
        }
    }

    public class DataPoint
    {
        int dist;
        bool distinct;
        public DataPoint(int distance, bool distinct)
        {
            dist = distance;
            this.distinct = distinct;
        }
    }
}
