﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ActuBoardAPI;

namespace VTExperiment1
{

    /// </summary>
    public partial class Calibration : Window
    {
        double sliderValue;
        private ActuBoardInterface _abi;
        public Calibration()
        {
            InitializeComponent();
            Condition condition = new Condition();
            condition.ShowDialog();
            condition.BringIntoView();
            _abi = new ActuBoardInterface();
            _abi.SetComPort(6);
            _abi.Connect();
            _abi.EnableOutput(true);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Console.WriteLine(motor + " " + sliderValue);
            //listbox.Items.Remove(listbox.Items[listbox.SelectedIndex]);

        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (listbox.SelectedItem != null)
            {
                //motor = ((Motor)listbox.SelectedItem).value;
                //Console.WriteLine(motor);
            }
        }

        private void overDrive(int channel)
        {
            _abi.SetChannel(channel, 120);
            System.Threading.Thread.Sleep(5);
        }
        
        //Console.Write("Setting motor " + motor + " intensity" + (int) ((1 - (sliderValue / 10)) * 255));
        private void slider1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            overDrive(40);
            int intensity = (int)((slider1.Value / 10) * 165);
            _abi.SetChannel(40, intensity);
            V1.Text = (int)(slider1.Value * 10) + "";
        }

        private void slider2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int intensity = (int)((slider2.Value / 10) * 165);
            _abi.SetChannel(41, intensity);
            V2.Text = (int)(slider2.Value * 10) + "";
        }

        private void slider3_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int intensity = (int)((slider3.Value / 10) * 165);
            _abi.SetChannel(42, intensity);
            V3.Text = (int)(slider3.Value * 10) + "";
        }

        private void slider4_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int intensity = (int)((slider4.Value / 10) * 165);
            _abi.SetChannel(43, intensity);
            V4.Text = (int)(slider4.Value * 10) + "";
        }

        private void slider5_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int intensity = (int)((slider5.Value / 10) * 165);
            _abi.SetChannel(44, intensity);
            V5.Text = (int)(slider5.Value * 10) + "";
        }

        private void slider6_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int intensity = (int)((slider6.Value / 10) * 165);
            _abi.SetChannel(45, intensity);
            V6.Text = (int)(slider6.Value * 10) + "";
        }

        private void slider7_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int intensity = (int)((slider7.Value / 10) * 165);
            _abi.SetChannel(46, intensity);
            V7.Text = (int)(slider7.Value * 10) + "";
        }

        private void slider8_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int intensity = (int)((slider8.Value / 10) * 165);
            _abi.SetChannel(47, intensity);
            V8.Text = (int)(slider8.Value * 10) + "";
        }

        private void slider9_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int intensity = (int)((slider9.Value / 10) * 165);
            _abi.SetChannel(48, intensity);
            V9.Text = (int)(slider9.Value * 10) + "";
        }

        private void slider10_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int intensity = (int)((slider10.Value / 10) * 165);
            _abi.SetChannel(49, intensity);
            V10.Text = (int)(slider10.Value * 10) + "";
        }

        private void C1_Checked(object sender, RoutedEventArgs e)
        {
            slider1.IsEnabled = false;
            _abi.SetChannel(40, 0);
        }

        private void C1_Unchecked(object sender, RoutedEventArgs e)
        {
            slider1.IsEnabled = true;
            int intensity = (int)((slider1.Value / 10) * 165);
            _abi.SetChannel(40, intensity);
        }

        private void C2_Checked(object sender, RoutedEventArgs e)
        {
            slider2.IsEnabled = false;
            _abi.SetChannel(41, 0);
        }

        private void C2_Unchecked(object sender, RoutedEventArgs e)
        {
            slider2.IsEnabled = true;
            int intensity = (int)((slider2.Value / 10) * 165);
            _abi.SetChannel(41, intensity);
        }

        private void C3_Checked(object sender, RoutedEventArgs e)
        {
            slider3.IsEnabled = false;
            _abi.SetChannel(42, 0);
        }

        private void C3_Unchecked(object sender, RoutedEventArgs e)
        {
            slider3.IsEnabled = true;
            int intensity = (int)((slider3.Value / 10) * 165);
            _abi.SetChannel(42, intensity);
        }

        private void C4_Checked(object sender, RoutedEventArgs e)
        {
            slider4.IsEnabled = false;
            _abi.SetChannel(43, 0);
        }

        private void C4_Unchecked(object sender, RoutedEventArgs e)
        {
            slider4.IsEnabled = true;
            int intensity = (int)((slider4.Value / 10) * 165);
            _abi.SetChannel(43, intensity);
        }

        private void C5_Checked(object sender, RoutedEventArgs e)
        {
            slider5.IsEnabled = false;
            _abi.SetChannel(44, 0);
        }

        private void C5_Unchecked(object sender, RoutedEventArgs e)
        {
            slider5.IsEnabled = true;
            int intensity = (int)((slider5.Value / 10) * 165);
            _abi.SetChannel(44, intensity);
        }

        private void C6_Checked(object sender, RoutedEventArgs e)
        {
            slider6.IsEnabled = false;
            _abi.SetChannel(45, 0);
        }

        private void C6_Unchecked(object sender, RoutedEventArgs e)
        {
            slider6.IsEnabled = true;
            int intensity = (int)((slider6.Value / 10) * 165);
            _abi.SetChannel(45, intensity);
        }

        private void C7_Checked(object sender, RoutedEventArgs e)
        {
            slider7.IsEnabled = false;
            _abi.SetChannel(46, 0);
        }

        private void C7_Unchecked(object sender, RoutedEventArgs e)
        {
            slider7.IsEnabled = true;
            int intensity = (int)((slider7.Value / 10) * 165);
            _abi.SetChannel(46, intensity);
        }

        private void C8_Checked(object sender, RoutedEventArgs e)
        {
            slider8.IsEnabled = false;
            _abi.SetChannel(47, 0);
        }

        private void C8_Unchecked(object sender, RoutedEventArgs e)
        {
            slider8.IsEnabled = true;
            int intensity = (int)((slider8.Value / 10) * 165);
            _abi.SetChannel(47, intensity);
        }

        private void C9_Checked(object sender, RoutedEventArgs e)
        {
            slider9.IsEnabled = false;
            _abi.SetChannel(48, 0);
        }

        private void C9_Unchecked(object sender, RoutedEventArgs e)
        {
            slider9.IsEnabled = true;
            int intensity = (int)((slider9.Value / 10) * 165);
            _abi.SetChannel(48, intensity);
        }

        private void C10_Checked(object sender, RoutedEventArgs e)
        {
            slider10.IsEnabled = false;
            _abi.SetChannel(49, 0);

        }

        private void C10_Unchecked(object sender, RoutedEventArgs e)
        {
            slider10.IsEnabled = true;
            int intensity = (int)((slider10.Value / 10) * 165);
            _abi.SetChannel(49, intensity);
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            foreach (double d in GetCalibrationData())
                Logger.logger.LogWrite(d + "", "Calibration");
            _abi.Disconnect();
        }
        
        public double[] GetCalibrationData()
        {
            return new double[10] { slider1.Value * 10, slider2.Value * 10, slider3.Value * 10, slider4.Value * 10, slider5.Value * 10, slider6.Value * 10, slider7.Value * 10, slider8.Value * 10, slider9.Value * 10, slider10.Value * 10 };
        }
    }

    public class Motor : ListItem
    {


        public string name;
        public int value;

        public Motor(int num)
        {
            name = "Motor " + num;
            value = num;
        }


        public override string ToString()
        {
            return this.name;
            
        }

    }
}
