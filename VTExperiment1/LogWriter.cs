﻿using System;
using System.IO;
using System.Reflection;


public class LogWriter
{
    private string m_exePath = string.Empty;
    private string Participant;
    public string BodyLocation;
    private string Orientation;

    public LogWriter(String Participant, String BodyLocation, String Orientation)
    {
        this.Participant = Participant;
        this.BodyLocation = BodyLocation;
        this.Orientation = Orientation;
    }

    public LogWriter(string logMessage)
    {
        LogWrite(logMessage);
    }
    public void LogWrite(string logMessage)
    {
        m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        m_exePath += "\\" + Participant;
        System.IO.Directory.CreateDirectory(m_exePath);
        try
        {
            using (StreamWriter w = File.AppendText(m_exePath + "\\" + Participant + "_" + BodyLocation + "_" + Orientation +".txt"))
            {
                Log(logMessage, w);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void LogWrite(string logMessage, string filename)
    {
        m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        m_exePath += "\\" + Participant;
        System.IO.Directory.CreateDirectory(m_exePath);
        try
        {
            using (StreamWriter w = File.AppendText(m_exePath + "\\" + filename + ".txt"))
            {
                Log(logMessage, w);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void Log(string logMessage, TextWriter txtWriter)
    {
        try
        {
            txtWriter.Write("\r\nLog Entry : ");
            txtWriter.WriteLine("{0}", DateTime.Now);
            txtWriter.WriteLine("  :{0}", logMessage);
            txtWriter.WriteLine("-------------------------------");
        }
        catch (Exception ex)
        {
        }
    }
}